// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_recipes.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CategoryRecipes _$$_CategoryRecipesFromJson(Map<String, dynamic> json) =>
    _$_CategoryRecipes(
      meals: (json['meals'] as List<dynamic>)
          .map((e) => Meals.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_CategoryRecipesToJson(_$_CategoryRecipes instance) =>
    <String, dynamic>{
      'meals': instance.meals,
    };

_$_Meals _$$_MealsFromJson(Map<String, dynamic> json) => _$_Meals(
      strMeal: json['strMeal'] as String,
      strMealThumb: json['strMealThumb'] as String,
      idMeal: json['idMeal'] as String,
    );

Map<String, dynamic> _$$_MealsToJson(_$_Meals instance) => <String, dynamic>{
      'strMeal': instance.strMeal,
      'strMealThumb': instance.strMealThumb,
      'idMeal': instance.idMeal,
    };
