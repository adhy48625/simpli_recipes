import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
part 'recipe.freezed.dart';
part 'recipe.g.dart';

@freezed
class Recipe with _$Recipe {
  const factory Recipe({
    required List<Meals> meals,
  }) = _Recipe;

  factory Recipe.fromJson(Map<String, Object?> json) => _$RecipeFromJson(json);
}

@freezed
class Meals with _$Meals {
  const factory Meals({
    required String strMeal,
    required String strMealThumb,
    required String idMeal,
    required String strArea,
    required String strInstructions,
    String? strTags,
    required String strYoutube,
    required String strIngredient1,
    required String strIngredient2,
    required String strIngredient3,
    required String strIngredient4,
    required String strIngredient5,
    required String strIngredient6,
    String? strIngredient7,
    String? strIngredient8,
    String? strIngredient9,
    String? strIngredient10,
    String? strIngredient11,
    String? strIngredient12,
    String? strIngredient13,
    String? strIngredient14,
    String? strIngredient15,
    String? strIngredient16,
    String? strIngredient17,
    String? strIngredient18,
    String? strIngredient19,
    String? strIngredient20,
    required String strMeasure1,
    required String strMeasure2,
    required String strMeasure3,
    required String strMeasure4,
    required String strMeasure5,
    required String strMeasure6,
    String? strMeasure7,
    String? strMeasure8,
    String? strMeasure9,
    String? strMeasure10,
    String? strMeasure11,
    String? strMeasure12,
    String? strMeasure13,
    String? strMeasure14,
    String? strMeasure15,
    String? strMeasure16,
    String? strMeasure17,
    String? strMeasure18,
    String? strMeasure19,
    String? strMeasure20,
  }) = _Meals;

  factory Meals.fromJson(Map<String, Object?> json) => _$MealsFromJson(json);
}
