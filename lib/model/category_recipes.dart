import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
part 'category_recipes.freezed.dart';
part 'category_recipes.g.dart';

@freezed
class CategoryRecipes with _$CategoryRecipes {
  const factory CategoryRecipes({
    required List<Meals> meals,
  }) = _CategoryRecipes;

  factory CategoryRecipes.fromJson(Map<String, Object?> json) => _$CategoryRecipesFromJson(json);
}

@freezed
class Meals with _$Meals {
  const factory Meals({
    required String strMeal,
    required String strMealThumb,
    required String idMeal,
  }) = _Meals;

  factory Meals.fromJson(Map<String, Object?> json) => _$MealsFromJson(json);
}

// class RandomMeals {
//   final List<RandomFood> meals;

//   RandomMeals({
//     required this.meals,
//   });

//   factory RandomMeals.fromJson(Map<String, dynamic> json) {
//     return RandomMeals(
//       meals: (json['meals'] as List<dynamic>)
//           .map((meal) => RandomFood.fromJson(meal))
//           .toList() ,
//     );
//   }
// }

// class RandomFood {
//   final String strMeal;
//   final String strMealThumb;
//   final String idMeal;
//   final String strCategory;
//   final String strInstructions;
//   final String strTags;
//   final String strYoutube;

//   RandomFood({
//     required this.strMeal,
//     required this.strMealThumb,
//     required this.idMeal,
//     required this.strCategory,
//     required this.strInstructions,
//     required this.strTags,
//     required this.strYoutube,
//   });

//   factory RandomFood.fromJson(Map<String, dynamic> json) {
//     return RandomFood(
//       strMeal: json['strMeal'],
//       strMealThumb: json['strMealThumb'],
//       idMeal: json['idMeal'],
//       strCategory: json['strCategory'],
//       strInstructions: json['strInstructions'],
//       strTags: json['strTags']??'',
//       strYoutube: json['strYoutube'],
//     );
//   }
// }