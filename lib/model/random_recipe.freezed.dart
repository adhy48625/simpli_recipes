// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'random_recipe.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RandomRecipe _$RandomRecipeFromJson(Map<String, dynamic> json) {
  return _RandomRecipe.fromJson(json);
}

/// @nodoc
mixin _$RandomRecipe {
  List<Meals> get meals => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RandomRecipeCopyWith<RandomRecipe> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RandomRecipeCopyWith<$Res> {
  factory $RandomRecipeCopyWith(
          RandomRecipe value, $Res Function(RandomRecipe) then) =
      _$RandomRecipeCopyWithImpl<$Res, RandomRecipe>;
  @useResult
  $Res call({List<Meals> meals});
}

/// @nodoc
class _$RandomRecipeCopyWithImpl<$Res, $Val extends RandomRecipe>
    implements $RandomRecipeCopyWith<$Res> {
  _$RandomRecipeCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? meals = null,
  }) {
    return _then(_value.copyWith(
      meals: null == meals
          ? _value.meals
          : meals // ignore: cast_nullable_to_non_nullable
              as List<Meals>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_RandomRecipeCopyWith<$Res>
    implements $RandomRecipeCopyWith<$Res> {
  factory _$$_RandomRecipeCopyWith(
          _$_RandomRecipe value, $Res Function(_$_RandomRecipe) then) =
      __$$_RandomRecipeCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<Meals> meals});
}

/// @nodoc
class __$$_RandomRecipeCopyWithImpl<$Res>
    extends _$RandomRecipeCopyWithImpl<$Res, _$_RandomRecipe>
    implements _$$_RandomRecipeCopyWith<$Res> {
  __$$_RandomRecipeCopyWithImpl(
      _$_RandomRecipe _value, $Res Function(_$_RandomRecipe) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? meals = null,
  }) {
    return _then(_$_RandomRecipe(
      meals: null == meals
          ? _value._meals
          : meals // ignore: cast_nullable_to_non_nullable
              as List<Meals>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_RandomRecipe with DiagnosticableTreeMixin implements _RandomRecipe {
  const _$_RandomRecipe({required final List<Meals> meals}) : _meals = meals;

  factory _$_RandomRecipe.fromJson(Map<String, dynamic> json) =>
      _$$_RandomRecipeFromJson(json);

  final List<Meals> _meals;
  @override
  List<Meals> get meals {
    if (_meals is EqualUnmodifiableListView) return _meals;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_meals);
  }

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RandomRecipe(meals: $meals)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'RandomRecipe'))
      ..add(DiagnosticsProperty('meals', meals));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RandomRecipe &&
            const DeepCollectionEquality().equals(other._meals, _meals));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_meals));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RandomRecipeCopyWith<_$_RandomRecipe> get copyWith =>
      __$$_RandomRecipeCopyWithImpl<_$_RandomRecipe>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_RandomRecipeToJson(
      this,
    );
  }
}

abstract class _RandomRecipe implements RandomRecipe {
  const factory _RandomRecipe({required final List<Meals> meals}) =
      _$_RandomRecipe;

  factory _RandomRecipe.fromJson(Map<String, dynamic> json) =
      _$_RandomRecipe.fromJson;

  @override
  List<Meals> get meals;
  @override
  @JsonKey(ignore: true)
  _$$_RandomRecipeCopyWith<_$_RandomRecipe> get copyWith =>
      throw _privateConstructorUsedError;
}

Meals _$MealsFromJson(Map<String, dynamic> json) {
  return _Meals.fromJson(json);
}

/// @nodoc
mixin _$Meals {
  String get strMeal => throw _privateConstructorUsedError;
  String get strMealThumb => throw _privateConstructorUsedError;
  String get idMeal => throw _privateConstructorUsedError;
  String get strArea => throw _privateConstructorUsedError;
  String get strInstructions => throw _privateConstructorUsedError;
  String get strCategory => throw _privateConstructorUsedError;
  String? get strTags => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MealsCopyWith<Meals> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MealsCopyWith<$Res> {
  factory $MealsCopyWith(Meals value, $Res Function(Meals) then) =
      _$MealsCopyWithImpl<$Res, Meals>;
  @useResult
  $Res call(
      {String strMeal,
      String strMealThumb,
      String idMeal,
      String strArea,
      String strInstructions,
      String strCategory,
      String? strTags});
}

/// @nodoc
class _$MealsCopyWithImpl<$Res, $Val extends Meals>
    implements $MealsCopyWith<$Res> {
  _$MealsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? strMeal = null,
    Object? strMealThumb = null,
    Object? idMeal = null,
    Object? strArea = null,
    Object? strInstructions = null,
    Object? strCategory = null,
    Object? strTags = freezed,
  }) {
    return _then(_value.copyWith(
      strMeal: null == strMeal
          ? _value.strMeal
          : strMeal // ignore: cast_nullable_to_non_nullable
              as String,
      strMealThumb: null == strMealThumb
          ? _value.strMealThumb
          : strMealThumb // ignore: cast_nullable_to_non_nullable
              as String,
      idMeal: null == idMeal
          ? _value.idMeal
          : idMeal // ignore: cast_nullable_to_non_nullable
              as String,
      strArea: null == strArea
          ? _value.strArea
          : strArea // ignore: cast_nullable_to_non_nullable
              as String,
      strInstructions: null == strInstructions
          ? _value.strInstructions
          : strInstructions // ignore: cast_nullable_to_non_nullable
              as String,
      strCategory: null == strCategory
          ? _value.strCategory
          : strCategory // ignore: cast_nullable_to_non_nullable
              as String,
      strTags: freezed == strTags
          ? _value.strTags
          : strTags // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MealsCopyWith<$Res> implements $MealsCopyWith<$Res> {
  factory _$$_MealsCopyWith(_$_Meals value, $Res Function(_$_Meals) then) =
      __$$_MealsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String strMeal,
      String strMealThumb,
      String idMeal,
      String strArea,
      String strInstructions,
      String strCategory,
      String? strTags});
}

/// @nodoc
class __$$_MealsCopyWithImpl<$Res> extends _$MealsCopyWithImpl<$Res, _$_Meals>
    implements _$$_MealsCopyWith<$Res> {
  __$$_MealsCopyWithImpl(_$_Meals _value, $Res Function(_$_Meals) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? strMeal = null,
    Object? strMealThumb = null,
    Object? idMeal = null,
    Object? strArea = null,
    Object? strInstructions = null,
    Object? strCategory = null,
    Object? strTags = freezed,
  }) {
    return _then(_$_Meals(
      strMeal: null == strMeal
          ? _value.strMeal
          : strMeal // ignore: cast_nullable_to_non_nullable
              as String,
      strMealThumb: null == strMealThumb
          ? _value.strMealThumb
          : strMealThumb // ignore: cast_nullable_to_non_nullable
              as String,
      idMeal: null == idMeal
          ? _value.idMeal
          : idMeal // ignore: cast_nullable_to_non_nullable
              as String,
      strArea: null == strArea
          ? _value.strArea
          : strArea // ignore: cast_nullable_to_non_nullable
              as String,
      strInstructions: null == strInstructions
          ? _value.strInstructions
          : strInstructions // ignore: cast_nullable_to_non_nullable
              as String,
      strCategory: null == strCategory
          ? _value.strCategory
          : strCategory // ignore: cast_nullable_to_non_nullable
              as String,
      strTags: freezed == strTags
          ? _value.strTags
          : strTags // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Meals with DiagnosticableTreeMixin implements _Meals {
  const _$_Meals(
      {required this.strMeal,
      required this.strMealThumb,
      required this.idMeal,
      required this.strArea,
      required this.strInstructions,
      required this.strCategory,
      this.strTags});

  factory _$_Meals.fromJson(Map<String, dynamic> json) =>
      _$$_MealsFromJson(json);

  @override
  final String strMeal;
  @override
  final String strMealThumb;
  @override
  final String idMeal;
  @override
  final String strArea;
  @override
  final String strInstructions;
  @override
  final String strCategory;
  @override
  final String? strTags;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Meals(strMeal: $strMeal, strMealThumb: $strMealThumb, idMeal: $idMeal, strArea: $strArea, strInstructions: $strInstructions, strCategory: $strCategory, strTags: $strTags)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Meals'))
      ..add(DiagnosticsProperty('strMeal', strMeal))
      ..add(DiagnosticsProperty('strMealThumb', strMealThumb))
      ..add(DiagnosticsProperty('idMeal', idMeal))
      ..add(DiagnosticsProperty('strArea', strArea))
      ..add(DiagnosticsProperty('strInstructions', strInstructions))
      ..add(DiagnosticsProperty('strCategory', strCategory))
      ..add(DiagnosticsProperty('strTags', strTags));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Meals &&
            (identical(other.strMeal, strMeal) || other.strMeal == strMeal) &&
            (identical(other.strMealThumb, strMealThumb) ||
                other.strMealThumb == strMealThumb) &&
            (identical(other.idMeal, idMeal) || other.idMeal == idMeal) &&
            (identical(other.strArea, strArea) || other.strArea == strArea) &&
            (identical(other.strInstructions, strInstructions) ||
                other.strInstructions == strInstructions) &&
            (identical(other.strCategory, strCategory) ||
                other.strCategory == strCategory) &&
            (identical(other.strTags, strTags) || other.strTags == strTags));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, strMeal, strMealThumb, idMeal,
      strArea, strInstructions, strCategory, strTags);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MealsCopyWith<_$_Meals> get copyWith =>
      __$$_MealsCopyWithImpl<_$_Meals>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MealsToJson(
      this,
    );
  }
}

abstract class _Meals implements Meals {
  const factory _Meals(
      {required final String strMeal,
      required final String strMealThumb,
      required final String idMeal,
      required final String strArea,
      required final String strInstructions,
      required final String strCategory,
      final String? strTags}) = _$_Meals;

  factory _Meals.fromJson(Map<String, dynamic> json) = _$_Meals.fromJson;

  @override
  String get strMeal;
  @override
  String get strMealThumb;
  @override
  String get idMeal;
  @override
  String get strArea;
  @override
  String get strInstructions;
  @override
  String get strCategory;
  @override
  String? get strTags;
  @override
  @JsonKey(ignore: true)
  _$$_MealsCopyWith<_$_Meals> get copyWith =>
      throw _privateConstructorUsedError;
}
