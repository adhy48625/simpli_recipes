import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';
part 'random_recipe.freezed.dart';
part 'random_recipe.g.dart';

@freezed
class RandomRecipe with _$RandomRecipe {
  const factory RandomRecipe({
    required List<Meals> meals,
  }) = _RandomRecipe;

  factory RandomRecipe.fromJson(Map<String, Object?> json) => _$RandomRecipeFromJson(json);
}

@freezed
class Meals with _$Meals {
  const factory Meals({
    required String strMeal,
    required String strMealThumb,
    required String idMeal,
    required String strArea,
    required String strInstructions,
    required String strCategory,
    String? strTags,
    // required String strYoutube,
    // required String strIngredient1,
    // required String strIngredient2,
    // required String strIngredient3,
    // required String strIngredient4,
    // required String strIngredient5,
    // required String strIngredient6,
    // String? strIngredient7,
    // String? strIngredient8,
    // String? strIngredient9,
    // String? strIngredient10,
    // String? strIngredient11,
    // String? strIngredient12,
    // String? strIngredient13,
    // String? strIngredient14,
    // String? strIngredient15,
    // String? strIngredient16,
    // String? strIngredient17,
    // String? strIngredient18,
    // String? strIngredient19,
    // String? strIngredient20,
  }) = _Meals;

  factory Meals.fromJson(Map<String, Object?> json) => _$MealsFromJson(json);
}
