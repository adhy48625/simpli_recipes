// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'random_recipe.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_RandomRecipe _$$_RandomRecipeFromJson(Map<String, dynamic> json) =>
    _$_RandomRecipe(
      meals: (json['meals'] as List<dynamic>)
          .map((e) => Meals.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_RandomRecipeToJson(_$_RandomRecipe instance) =>
    <String, dynamic>{
      'meals': instance.meals,
    };

_$_Meals _$$_MealsFromJson(Map<String, dynamic> json) => _$_Meals(
      strMeal: json['strMeal'] as String,
      strMealThumb: json['strMealThumb'] as String,
      idMeal: json['idMeal'] as String,
      strArea: json['strArea'] as String,
      strInstructions: json['strInstructions'] as String,
      strCategory: json['strCategory'] as String,
      strTags: json['strTags'] as String?,
    );

Map<String, dynamic> _$$_MealsToJson(_$_Meals instance) => <String, dynamic>{
      'strMeal': instance.strMeal,
      'strMealThumb': instance.strMealThumb,
      'idMeal': instance.idMeal,
      'strArea': instance.strArea,
      'strInstructions': instance.strInstructions,
      'strCategory': instance.strCategory,
      'strTags': instance.strTags,
    };
