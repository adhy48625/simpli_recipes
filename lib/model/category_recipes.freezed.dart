// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'category_recipes.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CategoryRecipes _$CategoryRecipesFromJson(Map<String, dynamic> json) {
  return _CategoryRecipes.fromJson(json);
}

/// @nodoc
mixin _$CategoryRecipes {
  List<Meals> get meals => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CategoryRecipesCopyWith<CategoryRecipes> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoryRecipesCopyWith<$Res> {
  factory $CategoryRecipesCopyWith(
          CategoryRecipes value, $Res Function(CategoryRecipes) then) =
      _$CategoryRecipesCopyWithImpl<$Res, CategoryRecipes>;
  @useResult
  $Res call({List<Meals> meals});
}

/// @nodoc
class _$CategoryRecipesCopyWithImpl<$Res, $Val extends CategoryRecipes>
    implements $CategoryRecipesCopyWith<$Res> {
  _$CategoryRecipesCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? meals = null,
  }) {
    return _then(_value.copyWith(
      meals: null == meals
          ? _value.meals
          : meals // ignore: cast_nullable_to_non_nullable
              as List<Meals>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CategoryRecipesCopyWith<$Res>
    implements $CategoryRecipesCopyWith<$Res> {
  factory _$$_CategoryRecipesCopyWith(
          _$_CategoryRecipes value, $Res Function(_$_CategoryRecipes) then) =
      __$$_CategoryRecipesCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<Meals> meals});
}

/// @nodoc
class __$$_CategoryRecipesCopyWithImpl<$Res>
    extends _$CategoryRecipesCopyWithImpl<$Res, _$_CategoryRecipes>
    implements _$$_CategoryRecipesCopyWith<$Res> {
  __$$_CategoryRecipesCopyWithImpl(
      _$_CategoryRecipes _value, $Res Function(_$_CategoryRecipes) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? meals = null,
  }) {
    return _then(_$_CategoryRecipes(
      meals: null == meals
          ? _value._meals
          : meals // ignore: cast_nullable_to_non_nullable
              as List<Meals>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CategoryRecipes
    with DiagnosticableTreeMixin
    implements _CategoryRecipes {
  const _$_CategoryRecipes({required final List<Meals> meals}) : _meals = meals;

  factory _$_CategoryRecipes.fromJson(Map<String, dynamic> json) =>
      _$$_CategoryRecipesFromJson(json);

  final List<Meals> _meals;
  @override
  List<Meals> get meals {
    if (_meals is EqualUnmodifiableListView) return _meals;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_meals);
  }

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CategoryRecipes(meals: $meals)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'CategoryRecipes'))
      ..add(DiagnosticsProperty('meals', meals));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CategoryRecipes &&
            const DeepCollectionEquality().equals(other._meals, _meals));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_meals));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CategoryRecipesCopyWith<_$_CategoryRecipes> get copyWith =>
      __$$_CategoryRecipesCopyWithImpl<_$_CategoryRecipes>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CategoryRecipesToJson(
      this,
    );
  }
}

abstract class _CategoryRecipes implements CategoryRecipes {
  const factory _CategoryRecipes({required final List<Meals> meals}) =
      _$_CategoryRecipes;

  factory _CategoryRecipes.fromJson(Map<String, dynamic> json) =
      _$_CategoryRecipes.fromJson;

  @override
  List<Meals> get meals;
  @override
  @JsonKey(ignore: true)
  _$$_CategoryRecipesCopyWith<_$_CategoryRecipes> get copyWith =>
      throw _privateConstructorUsedError;
}

Meals _$MealsFromJson(Map<String, dynamic> json) {
  return _Meals.fromJson(json);
}

/// @nodoc
mixin _$Meals {
  String get strMeal => throw _privateConstructorUsedError;
  String get strMealThumb => throw _privateConstructorUsedError;
  String get idMeal => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MealsCopyWith<Meals> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MealsCopyWith<$Res> {
  factory $MealsCopyWith(Meals value, $Res Function(Meals) then) =
      _$MealsCopyWithImpl<$Res, Meals>;
  @useResult
  $Res call({String strMeal, String strMealThumb, String idMeal});
}

/// @nodoc
class _$MealsCopyWithImpl<$Res, $Val extends Meals>
    implements $MealsCopyWith<$Res> {
  _$MealsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? strMeal = null,
    Object? strMealThumb = null,
    Object? idMeal = null,
  }) {
    return _then(_value.copyWith(
      strMeal: null == strMeal
          ? _value.strMeal
          : strMeal // ignore: cast_nullable_to_non_nullable
              as String,
      strMealThumb: null == strMealThumb
          ? _value.strMealThumb
          : strMealThumb // ignore: cast_nullable_to_non_nullable
              as String,
      idMeal: null == idMeal
          ? _value.idMeal
          : idMeal // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MealsCopyWith<$Res> implements $MealsCopyWith<$Res> {
  factory _$$_MealsCopyWith(_$_Meals value, $Res Function(_$_Meals) then) =
      __$$_MealsCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String strMeal, String strMealThumb, String idMeal});
}

/// @nodoc
class __$$_MealsCopyWithImpl<$Res> extends _$MealsCopyWithImpl<$Res, _$_Meals>
    implements _$$_MealsCopyWith<$Res> {
  __$$_MealsCopyWithImpl(_$_Meals _value, $Res Function(_$_Meals) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? strMeal = null,
    Object? strMealThumb = null,
    Object? idMeal = null,
  }) {
    return _then(_$_Meals(
      strMeal: null == strMeal
          ? _value.strMeal
          : strMeal // ignore: cast_nullable_to_non_nullable
              as String,
      strMealThumb: null == strMealThumb
          ? _value.strMealThumb
          : strMealThumb // ignore: cast_nullable_to_non_nullable
              as String,
      idMeal: null == idMeal
          ? _value.idMeal
          : idMeal // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Meals with DiagnosticableTreeMixin implements _Meals {
  const _$_Meals(
      {required this.strMeal,
      required this.strMealThumb,
      required this.idMeal});

  factory _$_Meals.fromJson(Map<String, dynamic> json) =>
      _$$_MealsFromJson(json);

  @override
  final String strMeal;
  @override
  final String strMealThumb;
  @override
  final String idMeal;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Meals(strMeal: $strMeal, strMealThumb: $strMealThumb, idMeal: $idMeal)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Meals'))
      ..add(DiagnosticsProperty('strMeal', strMeal))
      ..add(DiagnosticsProperty('strMealThumb', strMealThumb))
      ..add(DiagnosticsProperty('idMeal', idMeal));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Meals &&
            (identical(other.strMeal, strMeal) || other.strMeal == strMeal) &&
            (identical(other.strMealThumb, strMealThumb) ||
                other.strMealThumb == strMealThumb) &&
            (identical(other.idMeal, idMeal) || other.idMeal == idMeal));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, strMeal, strMealThumb, idMeal);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MealsCopyWith<_$_Meals> get copyWith =>
      __$$_MealsCopyWithImpl<_$_Meals>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MealsToJson(
      this,
    );
  }
}

abstract class _Meals implements Meals {
  const factory _Meals(
      {required final String strMeal,
      required final String strMealThumb,
      required final String idMeal}) = _$_Meals;

  factory _Meals.fromJson(Map<String, dynamic> json) = _$_Meals.fromJson;

  @override
  String get strMeal;
  @override
  String get strMealThumb;
  @override
  String get idMeal;
  @override
  @JsonKey(ignore: true)
  _$$_MealsCopyWith<_$_Meals> get copyWith =>
      throw _privateConstructorUsedError;
}
