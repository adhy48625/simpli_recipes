import 'package:flutter/material.dart';
import 'package:simpli_recipes/api/recipes.dart';
import 'package:simpli_recipes/model/recipe.dart';

class RecipeScreen extends StatefulWidget {
  const RecipeScreen({
    super.key,
    required this.id,
  });
  final String id;

  @override
  State<RecipeScreen> createState() => _RecipeScreenState();
}

class _RecipeScreenState extends State<RecipeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: FutureBuilder(
        future: fetchRecipe(widget.id),
        builder: (BuildContext context, AsyncSnapshot<Recipe> snapshot) {
          if (snapshot.hasData) {
            return SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const SizedBox(height: 10),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: Image.network(snapshot.data!.meals[0].strMealThumb, fit: BoxFit.fill, height: 200),
                    ),
                    const SizedBox(height: 10),
                    Text(
                      snapshot.data!.meals[0].strMeal,
                      style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text(snapshot.data!.meals[0].strInstructions),
                    ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: 20,
                      itemBuilder: (context, index) {
                        final ingredientKey = 'strIngredient${index + 1}';
                        final measureKey = 'strMeasure${index + 1}';
                        return snapshot.data!.meals[0].toJson()[ingredientKey] != ""
                            ? Card(
                                child: ListTile(
                                  title: Text(snapshot.data!.meals[0].toJson()[ingredientKey]),
                                  trailing: Text(snapshot.data!.meals[0].toJson()[measureKey]),
                                ),
                              )
                            : Container();
                      },
                    )
                  ],
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return Center(child: Text('${snapshot.error}'));
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
