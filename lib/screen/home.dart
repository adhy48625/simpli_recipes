import 'package:flutter/material.dart';
import 'package:simpli_recipes/model/category_recipes.dart';
import 'package:simpli_recipes/screen/recipe.dart';

import '../api/recipes.dart';
import '../model/random_recipe.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String category = 'Beef';
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: FutureBuilder(
          future: fetchRecipeCategory(category),
          builder: (BuildContext context, AsyncSnapshot<CategoryRecipes> snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: [
                  const Expanded(child: RandomCard()),
                  const SizedBox(height: 10),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        categoryScrool(selected: category == 'Beef', category: 'Beef'),
                        categoryScrool(selected: category == 'Chicken', category: 'Chicken'),
                        categoryScrool(selected: category == 'Dessert', category: 'Dessert'),
                        categoryScrool(selected: category == 'Lamb', category: 'Lamb'),
                        categoryScrool(selected: category == 'Miscellaneous', category: 'Miscellaneous'),
                        categoryScrool(selected: category == 'Pasta', category: 'Pasta'),
                        categoryScrool(selected: category == 'Pork', category: 'Pork'),
                        categoryScrool(selected: category == 'Seafood', category: 'Seafood'),
                        categoryScrool(selected: category == 'Side', category: 'Side'),
                        categoryScrool(selected: category == 'Vegan', category: 'Vegan'),
                        categoryScrool(selected: category == 'Vegetarian', category: 'Vegetarian'),
                        categoryScrool(selected: category == 'Breakfast', category: 'Breakfast'),
                        categoryScrool(selected: category == 'Goat', category: 'Goat'),
                      ],
                    ),
                  ),
                  const SizedBox(height: 10),
                  Expanded(
                    flex: 2,
                    child: CustomScrollView(
                      slivers: [
                        SliverGrid(
                          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                          delegate: SliverChildBuilderDelegate(
                            childCount: snapshot.data!.meals.length,
                            (context, index) {
                              return Stack(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.all(10),
                                    child: InkWell(
                                      child: snapshot.connectionState == ConnectionState.done
                                          ? ClipRRect(
                                              borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                                              child: ShaderMask(
                                                blendMode: BlendMode.darken, // Sesuaikan dengan kebutuhan Anda
                                                shaderCallback: (Rect bounds) {
                                                  return const LinearGradient(
                                                    begin: Alignment.topCenter,
                                                    end: Alignment.bottomCenter,
                                                    colors: [
                                                      Colors.transparent,
                                                      Colors.black,
                                                    ],
                                                  ).createShader(bounds);
                                                },
                                                child: Image.network(snapshot.data!.meals[index].strMealThumb, fit: BoxFit.cover),
                                              ),
                                            )
                                          : const Center(
                                              child: CircularProgressIndicator(),
                                            ),
                                      onTap: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => RecipeScreen(id: snapshot.data!.meals[index].idMeal),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    bottom: 40,
                                    left: 0,
                                    right: 0,
                                    child: Text(
                                      snapshot.data!.meals[index].strMeal,
                                      style: const TextStyle(color: Colors.white, fontSize: 10),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Center(child: Text('${snapshot.error}'));
            }
            return const Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }

  InkWell categoryScrool({String category = 'Beef', bool? selected}) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.all(8),
        margin: const EdgeInsets.symmetric(horizontal: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: selected! ? Colors.blue : Colors.grey[350], // Gunakan selected langsung untuk kondisi warna
        ),
        child: Text(category),
      ),
      onTap: () {
        if (this.category != category) {
          setState(() {
            this.category = category;
            selected = !selected!;
            fetchRecipeCategory(category);
          });
        }
      },
    );
  }
}

class RandomCard extends StatefulWidget {
  const RandomCard({
    super.key,
  });

  @override
  State<RandomCard> createState() => _RandomCardState();
}

class _RandomCardState extends State<RandomCard> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        children: [
          Align(
            alignment: Alignment.bottomLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const Text('Random Recipe'),
                IconButton(
                    onPressed: () {
                      setState(() {
                        fetchRecipeRandom();
                      });
                    },
                    icon: const Icon(Icons.refresh)),
              ],
            ),
          ),
          Expanded(
            child: FutureBuilder(
              future: fetchRecipeRandom(),
              builder: (BuildContext context, AsyncSnapshot<RandomRecipe> snapshot) {
                if (snapshot.hasData) {
                  return Stack(
                    children: [
                      InkWell(
                        child: snapshot.connectionState == ConnectionState.done
                            ? ClipRRect(
                                borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                                child: ShaderMask(
                                  blendMode: BlendMode.darken, // Sesuaikan dengan kebutuhan Anda
                                  shaderCallback: (Rect bounds) {
                                    return const LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        Colors.transparent,
                                        Colors.black,
                                      ],
                                    ).createShader(bounds);
                                  },
                                  child: SizedBox(
                                    height: 400,
                                    width: 300,
                                    child: Image.network(snapshot.data!.meals[0].strMealThumb, fit: BoxFit.cover),
                                  ),
                                ),
                              )
                            : const Center(
                                child: CircularProgressIndicator(),
                              ),
                        onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => RecipeScreen(id: snapshot.data!.meals[0].idMeal),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 10,
                        left: 0,
                        right: 0,
                        child: Text(
                          snapshot.data!.meals[0].strMeal,
                          style: const TextStyle(color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Center(child: Text('${snapshot.error}'));
                }
                return const Center(child: CircularProgressIndicator());
              },
            ),
          ),
        ],
      ),
    );
  }
}
