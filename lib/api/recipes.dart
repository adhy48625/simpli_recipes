import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:simpli_recipes/model/category_recipes.dart';

import '../model/random_recipe.dart';
import '../model/recipe.dart';

Future<CategoryRecipes> fetchRecipeCategory(category) async {
  final response = await http.get(Uri.parse('https://www.themealdb.com/api/json/v1/1/filter.php?c=$category'));
  var jsonObject = json.decode(response.body);
  if (response.statusCode == 200) {
    return CategoryRecipes.fromJson(jsonObject);
  } else {
    throw Exception('Failed to load recipe');
  }
}

Future<RandomRecipe> fetchRecipeRandom() async {
  final response = await http.get(Uri.parse('https://www.themealdb.com/api/json/v1/1/random.php'));
  var jsonObject = json.decode(response.body);
  if (response.statusCode == 200) {
    return RandomRecipe.fromJson(jsonObject);
  } else {
    throw Exception('Failed to load recipe');
  }
}

Future<Recipe> fetchRecipe(String id) async {
  final response = await http.get(Uri.parse('https://www.themealdb.com/api/json/v1/1/lookup.php?i=$id'));
  var jsonObject = json.decode(response.body);
  if (response.statusCode == 200) {
    return Recipe.fromJson(jsonObject);
  } else {
    throw Exception('Failed to load recipe');
  }
}
